# Midas app: openldap

An instance of the LDAP server [openldap](https://www.openldap.org/) with the web-based LDAP client [phpldapadmin](http://phpldapadmin.sourceforge.net/wiki/index.php/Main_Page)

This app can be used to deploy some Single Sign-On and manage user registration and groups access

This repository is part of the project **Midas** (for Minimalist Docker/Alpine Server). More infos in the [project wiki](https://gitlab.ensimag.fr/groups/midas/-/wikis/home).